#include "sharpen.hpp"

using namespace std;

Sharpen::Sharpen(){

}

void Sharpen::aplicaFiltro (Imagem &algumaImagem){
	int i,j,aux,num,x,y,div,size;
        int sharpen[]={0,-1,0,-1,5,-1,0,-1,0};
        int *m_aux_sh=new int[algumaImagem.getAltura()*algumaImagem.getLargura()];

        div=1;
        size=3;
        aux=size/2;

        for(i=aux;i<algumaImagem.getAltura()-aux;i++){
                for(j=aux;j<algumaImagem.getLargura()-aux;j++){
                        num=0;
                        
                        for(x=-1;x<=1;x++){
                                for(y=-1;y<=1;y++){
                                        num+=sharpen[(x+1)+size*(y+1)]*algumaImagem.getPixel(i+x,y+j);
                                }
                        }

                        num=num/div;
                 
                        num=num < 0 ? 0 : num;
                        num=num > 255 ? 255: num;
                
                        m_aux_sh[i+algumaImagem.getLargura()*j]=num;
                }
        }

        for(i=0;i<algumaImagem.getAltura();i++){
                for(j=0;j<algumaImagem.getLargura();j++){
                        algumaImagem.modPixel(i,j,m_aux_sh[i+algumaImagem.getLargura()*j]);
                }
        }

} 
