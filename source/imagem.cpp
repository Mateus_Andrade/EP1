#include "imagem.hpp"
#define MAX 100

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <sstream>

using namespace std;

//implementando construtores

Imagem::Imagem(){
	altura = 0;
	largura = 0;
	nivelCinza = 0;

	pixelVal = NULL;
}

Imagem::Imagem(int altura, int largura, int nivelCinza){
	this->altura = altura;
	this->largura = largura;
	this->nivelCinza = nivelCinza;
	
	pixelVal = new int [altura*largura];
}

Imagem::Imagem(const char arq_img[]){ //verificando a imagem
	char cabecalho[MAX],*ptr;
	ifstream imagempgm;

	imagempgm.open(arq_img, ios::in | ios::binary);

	if (!imagempgm){
		cout << "Impossível abrir a imagem" << endl;
		exit(1);
	}
	imagempgm.getline(cabecalho,MAX,'\n');
	if ((cabecalho[0]!='P') || (cabecalho[1]!='5')){
		cout << "A imagem nao e PGM" << endl;
	}
	while (cabecalho[0] == '#'){
		imagempgm.getline(cabecalho,MAX,'\n');
	}

	altura = strtol(cabecalho,&ptr,0);
	largura = atoi(ptr);

	imagempgm.getline(cabecalho,MAX,'\n');
	nivelCinza = strtol(cabecalho,&ptr,0);
	pixelVal = new int[altura*largura];
	cout << "Imagem OK" << endl;
}

int Imagem::getAltura(){

return altura;
}

void Imagem::setAltura(int altura){
	this->altura=altura;
}
int Imagem::getLargura(){

return largura;
}

void Imagem::setLargura(int largura){
	this->largura=largura;
}

int Imagem::getNivelCinza(){
return nivelCinza;
}

void Imagem::setNivelCinza(int nivelCinza){
	this->nivelCinza=nivelCinza;
}

void Imagem::leiaImg(const char img_file[]){
	int i,j;
	char cabecalho[MAX];
	char *ptr;
	unsigned char * tamPixels;
	ifstream imagempgm;

	imagempgm.open(img_file, ios::in | ios::binary);//abre imagempgm com o nome img_file

	imagempgm.getline(cabecalho,MAX,'\n');
	imagempgm.getline(cabecalho,MAX,'\n');

	while(cabecalho[0]=='#'){
		imagempgm.getline(cabecalho,MAX,'\n');
}
	altura = strtol(cabecalho,&ptr,0);
	largura = atoi(ptr);

	imagempgm.getline(cabecalho,MAX,'\n');

	nivelCinza = strtol(cabecalho, &ptr,0);
	
	tamPixels = (unsigned char *) new unsigned char[altura*largura];
	
	imagempgm.read(reinterpret_cast<char *>(tamPixels), (altura*largura)*sizeof(unsigned char));
	
	if(imagempgm.fail())
	{
		cout << "A imagem " << img_file << "tem tamanho errado!" << endl;
		exit(1);
	}
	
	imagempgm.close();
	
	int val; 
	
	for(i=0; i<altura; i++)
	{
		for(j=0; j<largura; j++)
		{
			val = (int)tamPixels[i*largura+j];
			pixelVal[i*largura+j] = val;
		}
	}
	
	delete [] tamPixels;
}

void Imagem::salvaImg(const char img_new[]){
	int i,j;
	unsigned char * tamPixels;
	ofstream saidaimg(img_new);
	
	tamPixels=(unsigned char *) new unsigned char[altura*largura];
	
	int val;
	
	for(i=0;i<altura;i++){
		for(j=0;j<largura;j++){
			val=pixelVal[i*largura+j];
			tamPixels[i*largura+j]=(unsigned char)val;
		}
	}
	
	if(!saidaimg.is_open()){
		cout << "Nao se pode abrir arquivo de saida " << img_new << endl;
		exit(1);
	}
	
	saidaimg << "P5" << endl;
	saidaimg << altura << " " << largura << endl;
	saidaimg << 255 << endl;
	
	saidaimg.write(reinterpret_cast<char *>(tamPixels), (altura*largura)*sizeof(unsigned char));
	
	cout << "Novo arquivo gerado" << endl;
	saidaimg.close();
}

int Imagem::getPixel(int i, int j){
	return pixelVal[i*getLargura()+j];
}

void Imagem::modPixel(int i, int j, int newPixel){
	pixelVal[i*getLargura()+j] = newPixel;
}
