#include "negativo.hpp"
#include "sharpen.hpp"
#include "smooth.hpp"
#include "imagem.hpp"

#include <iostream>

using namespace std;

int main() {

	Imagem novaImg("lena.pgm"); //verificando se a imagem é PGM
	Negativo filtroNegativo;
	Sharpen filtroSharpen;
	Smooth filtroSmooth;

	novaImg.leiaImg("lena.pgm");

	//Filtro Negativo
	filtroNegativo.aplicaFiltro(novaImg);
	novaImg.salvaImg("lenaNegativo.pgm");	

	//Filtro Sharpen
	filtroSharpen.aplicaFiltro(novaImg);
	novaImg.salvaImg("lenaSharpen.pgm");

	//Filtro Smooth
	filtroSmooth.aplicaFiltro(novaImg);
	novaImg.salvaImg("lenaSmooth.pgm");

	cout << endl << "Imagens geradas." << endl << endl;

return 0;
}
