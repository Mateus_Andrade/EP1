#include "filtro.hpp"
#include "negativo.hpp"

using namespace std;

Negativo::Negativo(){

}

void Negativo::aplicaFiltro(Imagem &algumaImagem){
        int i,j,aux;

        for(i=0;i<algumaImagem.getAltura();i++){
                for(j=0;j<algumaImagem.getLargura();j++){
                        aux = 255 - algumaImagem.getPixel(i,j);
                        algumaImagem.modPixel(i,j,aux);
                }
        }

}
