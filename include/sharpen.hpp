#ifndef SHARPEN_H
#define SHARPEN_H

#include "filtro.hpp"

using namespace std;

class Sharpen: public Filtro{
	public:
		Sharpen();
		void aplicaFiltro(Imagem &algumaImagem);
};

#endif
