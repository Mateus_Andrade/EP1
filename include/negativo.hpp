#ifndef NEGATIVO_H
#define NEGATIVO_H

#include "filtro.hpp"

using namespace std;

class Negativo: public Filtro{

	public:
		Negativo();
		void aplicaFiltro(Imagem &algumaImagem);
};

#endif
