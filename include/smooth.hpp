#ifndef SMOOTH_H
#define SMOOTH_H

#include "filtro.hpp"

using namespace std;

class Smooth : public Filtro{
	public:
		Smooth();
		void aplicaFiltro (Imagem &algumaImagem);
};

#endif
