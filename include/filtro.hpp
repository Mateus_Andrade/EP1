#ifndef FILTRO_H
#define FILTRO_H

#include "imagem.hpp"

using namespace std;

class Filtro{

	public:
		Filtro();
		virtual void aplicaFiltro() { };
};

#endif
