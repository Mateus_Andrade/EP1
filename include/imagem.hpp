#ifndef IMAGEM_H
#define IMAGEM_H

#include <string>
#include <stdlib.h>
#include <sstream>
#include <iostream>
class Imagem{
	private:
		int altura;
		int largura;
		int nivelCinza;
		int *pixelVal; 
	public:
//métodos construtores
		Imagem();
		Imagem(const char arq_img[]);
		Imagem(int altura, int largura, int nivelCinza);
//demais métodos
		int getAltura();
		void setAltura(int altura);
		int getLargura();
		void setLargura(int largura);
		int getNivelCinza();
		void setNivelCinza(int nivelCinza);
		int getPixel(int i, int j);
		void modPixel(int i, int j, int newPixel);
		void leiaImg(const char img_file[]);
		void salvaImg(const char img_new[]);


};

#endif
