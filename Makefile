BINFOLDER := bin/
INCFOLDER := include/
SRCFOLDER := source/
OBJFOLDER := obj/

CCP := g++
CFLAGS := -Wall -ansi -W

SRCFILES := $(wildcard source/*.cpp)

all: $(SRCFILES:source/%.cpp=obj/%.o)
	$(CCP) $(CFLAGS) obj/*.o -o bin/finalBinary -I./include
	
obj/%.o: source/%.cpp
	$(CCP) $(CFLAGS) -c $< -o $@ -I./include
	
.PHONY: clean

run: 
	 ./bin/finalBinary

clean:
	rm -rf obj/*
	rm -rf bin/*

	
